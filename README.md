# LaserBox - a transportable laser engraver in a roadcase

_This repository contains supporting files for the portable laser engraver system. Most of the actual code is in other sources and will be referenced from here. Users as recommended to use the latest content and instructions from those sources._

![alt text goes here](http://elder.ninja/wp-content/uploads/2019/04/D7C9736F-E6BD-4B7A-9B59-CD309CA4416C-575x383.jpeg)

The laser engraver in a roadcase uses a Raspberry Pi as an integrated print server. It is similar in nature to how OctoPrint may be used as a print server for 3D printers.

The primary software consists of the following:
* [CNCjs](https://cnc.js.org/) - GRBL g-code processor written in NodeJS _installed as a [Raspbian Stretch image](https://github.com/cncjs/cncjs-pi-raspbian/releases)
* cncjs-pendant-raspi-snes - addon to CNCjs to support a USB SNES clone game controller for laser engraver setup. The code is a mashup of the [cncjs-pendant-raspi-gpio](https://github.com/cncjs/cncjs-pendant-raspi-gpio) and functional code from [Carl Danley's node-gamepad](https://github.com/carldanley/node-gamepad) implementation
* [Browsepy](http://ergoithz.github.io/browsepy/) - a python based file manager
* [AutoHotspot](http://www.raspberryconnect.com/network/item/330-raspberry-pi-auto-wifi-hotspot-switch-internet) - a system service script (and instructions) for automatically switching the Raspberry Pi WiFi from connecting to a known network or setting up its own hotspot.
* [ro-root.sh](https://blockdev.io/read-only-rpi/) - a script (and instructions) for making the Raspberry Pi default to read-only access of the SD card
* [rpiro /rpirw](https://gitlab.com/bradanlane/hawkeyepi/tree/master/sysfiles) - useful scripts (and supporting files) for toggling read-only mode

_NOTE: it is highly recommended to enable the read-only capability only after getting everything else working. However, it is not too difficult to switch back to read-write mode by removing the `init=...` string from the boot line of` cmdline.txt`. This is detailed in the link above._

<!--
Additionally detains of the construction and configuration of the laser engraver in a roadcase project are detailed in an article at [Elder.Ninja](http://elder.ninja/p/catagory/bradan-lane-builds).
-->

### Personal Notes:

There were some surprises along the way.

The [Hawkeye Pi Camera](https://gitlab.com/bradanlane/hawkeyepi) project was the basis for the USB device and the read-only configuration. 
However, the Hawkeye Pi Camera had a 32GB USB devices formatted as FAT32. That setup worked out-of-the-box on the Raspberry Pi. 
The LaserBox has a 64GB USB device and it was formatted as NTFS. 
After trying lots of things, including the recomended solution of installing ntfs-3g package, the USB drive was replaced with a smaller one which was FAT32.

The AutoHotspot feature has both a `systemctrl` process and a script which runs periodically by a `cron` job. 
The idea is the Raspberry Pi will boot with the best choice WiFi configuration and automatically change if the WiFi environment changes. 
For some reason, the `systemctrl` process fails more often than not. While some debugging was attempted, 
the short-term solution was to increase the frequency of the `cron` job since it always seems to do the right thing.

Using the CNCjs Raspbian image was a time saver. However, it has meant upgrades have been somewhat painful. 
The odds are better if you follow the individual steps for setting up a Raspberry Pi - then updates and bug fixes are easy to install.

The cncjs-pednant-raspi-snes code was the result of hacking up the cncjs-pendant-raspi-gpio. 
The original idea was to create a pseudo-hat for the Raspberry Pi with momentary push buttons. 
The fabrication of the button board was taking longer than expected so the design switched to the game controller. 
The available SNES clone was mostly sitting around and was connected. 
The functional code came from [Carl Danley's node-gamepad](https://github.com/carldanley/node-gamepad) implementation. 
However, the SNES clone has very different button codes generated.

The Arduino Nano based controller does not natively support PWM for the laser. It also does not support limit switches. 
New connectors were added for each capability. The PWM was relatively easy. The 2-pin connector for the laser has the necessary third pin right next to it.
The limit switches required adding jumpers from the underside of the controller. Neither of these changes are required.

Engraving PCBs is done with the laser at 100% power. The testing process adjusted the movement speed to yield the best results.
The limit switches are used for the homing process. This too is not a requirement _(although it does look cool)_.
Without limit switches, the setup of the hardware 0,0 position is still quite easy. Before pressing the red button to power
the stepper motors, gently pull the gantry all the way to the front and move the laser assembly all the way to the left.
Now press the red button to power the laser and stepper motors. The lower left position will be the hardware 0,0 position.